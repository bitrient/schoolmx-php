<!DOCTYPE html>
<html>
  <head>
    {{-- Import Google Icon Font --}}
    {{-- <link href="http://fonts.googleapis.com/icon?family=Material-Design-Icons" rel="stylesheet"> --}}
    <link href="{{asset('font/iconfont/material-icons.css?family=Material+Icons')}}" rel="stylesheet">
    <title>Schoolmx - @yield('title')</title>
    {{-- Import materialize.css --}}
    <link href="{{asset('css/materialize.min.css')}}" rel="stylesheet" media="screen, projection" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />

    {{-- Let the browser know website is optimized for mobile --}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>

  <body class="@yield('color')">

    @yield('body')
    <script type="text/javascript" src="{{asset('js/jquery-2.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-validate/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/fuel-wizard.js')}}"></script>
  </body>
</html>
