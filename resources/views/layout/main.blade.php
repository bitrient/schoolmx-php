@extends('layout.base')

@section('body')
  <header class="side-nav-offset">
    <ul id="profile-dropdown" class="dropdown-content">
      <li><a href="#!" class="black-text waves-effect waves-teal">
        <i class="material-icons left">person_outline</i>{{trans('menu.profile')}}</a></li>
      <li><a href="#!" class="black-text waves-effect waves-teal">
        <i class="material-icons left">equalizer</i>{{trans('menu.settings')}}</a></li>
      <li class="divider"></li>
      <li><a href="{{route('logout')}}" class="red-text text-lighten-1
        waves-effect waves-teal">
        <i class="material-icons left">lock_outline</i>{{trans('auth.logout')}}</a></li>
    </ul>

    <nav>
      <div class="nav-wrapper">
        <a href="#" data-activates="nav-mobile" class="button-collapse">
          <i class="material-icons">menu</i></a>
        <ul id="" class="right">
          <li><a href="#!" class="dropdown-button"
            data-activates="profile-dropdown">
            <i class="material-icons left">account_circle</i>
            <i class="material-icons right">more_vert</i>
            <small>{{ trans('menu.welcome') }},</small>
  									{{ ucwords($__user->last_name. ' ' . $__user->first_name) }}
            </a>
          </li>
        </ul>

        @include('shared.sidenav')

      </div>
    </nav>

  </header>
  <main class="side-nav-offset">
    @yield('content')
  </main>

  <footer class="page-footer side-nav-offset">
    {{-- <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Footer Content</h5>
          <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Links</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div> --}}
    {{-- <div class="footer-copyright">
      <div class="contgainer">
      © 2014 Copyright Text
      <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
      </div>
    </div> --}}
  </footer>
@endsection
