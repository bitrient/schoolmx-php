@extends('layout.base')

@section('color', 'blue lighten-2')
@section('body')

  <main class="auth-wrapper ">
    <div class="auth-container container">
      {{-- Page content goes here --}}
      @yield('content')
    </div>
  </main>

@endsection
