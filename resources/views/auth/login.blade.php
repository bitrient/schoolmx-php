@extends('layout.auth')

@section('title', 'Login')
@section('version', 'v1.0.2')
@section('content')
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          {{--<span class="card-title">Login</span>--}}
          <div class="card-title center">
            <img height="100" src="{{asset('img/logo.jpg')}}" class="center-block">
            {{$app_name or 'SchoolMx'}}
          </div>
          {{-- <p>Please provide your username and password to login.</p> --}}
          <div class="row">
            @if(count($errors) > 0)
              <div class="col s12">
                <div class="card-panel red">
                  @foreach($errors->all() as $error)
                    <div class="white-text">{!!$error!!}</div>
                  @endforeach
                </div>
              </div>
            @endif
          <form role="form" id="login" action="{{route('process_login')}}" method="post" novalidate>
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">account_circle</i>
                <input class="validate" id="username" name="username" type="text" value="{{old('username')}}"/>
                <label for="username" >Username</label>
              </div>

              <div class="input-field col s12">
                <i class="material-icons prefix">lock</i>
                <input id="password" class="validate" name="password" type="password" />
                <label for="password" >Password</label>
              </div>

              <div class="input-field col s12">
                <input id="remember" name="remember" type="checkbox" />
                <label for="remember">Keep me signed in for two weeks</label>
              </div>
            </div>

            <div class="card-action">
              <div class="row">
                <div class="col s12 m5">
                  <button class="btn waves-effect waves-light" type="submit">Login
                    <i class="material-icons left">vpn_key</i>
                  </button>
                </div>
                <div class="col s12 m7">
                  <a href="{{route('forgot_password')}}">Forgot password?</a>
                  {{csrf_field()}}
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
