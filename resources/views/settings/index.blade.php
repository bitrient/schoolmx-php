@extends('layout.main')

@section('title', 'Settings')
@section('content')
<div class="row">
    <div class="col s12 m6 l4">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Session & Term</span>

                <div>term and session</div>
            </div>

            <div class="card-action">
                <a class="btn-flat waves-effect" href="#">Save Changes</a>
            </div>
        </div>
    </div>
</div>
@endsection