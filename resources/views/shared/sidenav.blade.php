<ul id="nav-mobile" class="side-nav fixed teal" style="width: 240px">
  <li class="logo"><a id="logo-container" href="#!" class="">
       SVG</a></li>
  <li class=""><a href="{{route('dashboard')}}" class="waves-effect
    waves-red"><i class="material-icons left">dashboard</i>
    {{trans('menu.dashboard')}}</a></li>
  <li class="">
    <ul class="collapsible collapsible-accordion">
      <li class="">
        <a class="collapsible-header  waves-effect waves-red">
          <i class="material-icons left">assignment</i>
          <i class="material-icons right">more_vert</i>
          {{trans('menu.academics')}}</a>
        <div class="collapsible-body">
          <ul>
            <li><a href="{{route('admissions')}}" class="waves-effect waves-red">
              <i class="material-icons left">chevron_right</i>
              <i class="material-icons left">group_add</i>
              {{trans('menu.admissions')}}</a>
            </li>
          </ul>
        </div>
      </li>

    </ul>
  </li>

  <li class="">
    <ul class="collapsible collapsible-accordion">
      <li class="">
        <a class="collapsible-header  waves-effect waves-red">
          <i class="material-icons left">slow_motion_video</i>
          <i class="material-icons right">more_vert</i>
          {{trans('menu.administration')}}</a>
        <div class="collapsible-body">
          <ul>
            <li><a href="{{route('settings')}}" class="waves-effect waves-red">
                <i class="material-icons left">chevron_right</i>
                <i class="material-icons left">settings_application</i>
                {{trans('menu.settings')}}</a>
            </li>
            <li><a href="#" class="waves-effect waves-red">
                <i class="material-icons left">chevron_right</i>
                <i class="material-icons left">equalizer</i>
                {{trans('menu.reports')}}</a>
            </li>

          </ul>
        </div>
      </li>

    </ul>
  </li>
</ul>
