<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="height" name="height" />
            <label for="height">{{trans('forms.height')}}</label>
        </div>
    </div>
    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="weight" name="weight" />
            <label for="weight">{{trans('forms.weight')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="religion">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="religion">{{trans('forms.religion')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="blood-group">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="blood-group">{{trans('forms.blood_group')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <div class="input-field col s12">
            <textarea id="allergies" class="materialize-textarea"
                      name="allergies" length="120"></textarea>
            <label for="allergies">{{trans('forms.allergies')}}</label>
        </div>
    </div>
</div>