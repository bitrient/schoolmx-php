

<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="first_name" name="first_name" />
            <label for="first_name">{{trans('forms.first_name')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="middle_name" name="middle_name" />
            <label for="middle_name">{{trans('forms.middle_name')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="last_name" name="last_name" />
            <label for="last_name">{{trans('forms.last_name')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="dob" name="date_of_birth" />
            <label for="dob">{{trans('forms.dob')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="gender">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="gender">{{trans('forms.gender')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="mother_tongue" name="mother_tongue" />
            <label for="mother_tongue">{{trans('forms.mother_tongue')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="country">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="country">{{trans('forms.country')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="state">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="state">{{trans('forms.state')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="city">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="city">{{trans('forms.city')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <div class="input-field col s12">
            <textarea id="address" class="materialize-textarea"
                      name="address" length="120"></textarea>
            <label for="address">{{trans('forms.address')}}</label>
        </div>
    </div>
</div>