@extends('layout.main')

@section('title', 'Student Admissions')
@section('content')
<div class="row">
    <div class="col s12">
        {{--<form role="form" action="" method="post">--}}
            <div class="card wizard" data-initialize="wizard" id="admission">
                <div class="card-content steps-container">
                    <span class="card-title">Admissions</span>
                    <div class="steps-container">
                        <ul class="steps pagination row">
                            <li data-step="1" data-name="student" class="active col s12 m2"><a href="#!">{{trans('forms.bio_data')}}</a></li>
                            <li data-step="2" data-name="guardian" class="col s12 m3"><a href="#!">{{trans('forms.guardian_sibling')}}</a></li>
                            <li data-step="3" data-name="academics" class="col s12 m2"><a href="#!">{{trans('forms.academics')}}</a></li>
                            <li data-step="4" data-name="additional" class="col s12 m3"><a href="#!">{{trans('forms.additional_info')}}</a></li>
                            <li data-step="5" data-name="summary" class="col s12 m2"><a href="#!">{{trans('forms.summary')}}</a></li>
                        </ul>
                    </div>

                    <div class="step-content">

                        <div class="step-pane active" data-step="1">
                            @include('admissions.biodata')
                        </div>


                        <div class="step-pane" data-step="2">
                            @include('admissions.guardian')
                        </div>

                        <div class="step-pane" data-step="3">
                            @include('admissions.academics')
                        </div>

                        <div class="step-pane" data-step="4">
                            @include('admissions.additional')
                        </div>

                        <div class="step-pane" data-step="5">
                            <h5>Summary</h5>
                            <p>Nori grape silver beet broccoli kombu beet greens fava bean potato quandong celery. Bunya nuts black-eyed pea prairie turnip leek lentil turnip greens parsnip. Sea lettuce lettuce water chestnut eggplant winter purslane fennel azuki bean earthnut pea sierra leone bologi leek soko chicory celtuce parsley jÃ­cama salsify. </p>
                        </div>

                    </div>
                </div>

                <div class="card-action actions">
                    <button class="btn btn-prev">
                        Previous<i class="material-icons left">chevron_left</i></button>
                    <button class="btn btn-next" data-last="Submit">
                        Next<i class="material-icons right">chevron_right</i> </button>
                </div>
            </div>
        {{--</form>--}}
    </div>
</div>
@endsection
