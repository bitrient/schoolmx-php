{{--<p>{{trans('forms.guardian_sibling_prompt')}}</p>--}}
{{--<div class="row">--}}
    {{--<div class="col s12 m6">--}}
        {{--<div class="input-field col s12">--}}
            {{--<input type="text" id="search" name="search" />--}}
            {{--<label for="search">{{trans('forms.search')}}</label>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="col s12 m6">--}}
        {{--<div class="input-field col s12">--}}
            {{--<button class="btn btn-flat waves-effect">--}}
                {{--<i class="material-icons left">supervisor_account</i> {{trans('forms.add_guardian')}}</button>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="title">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="title">{{trans('forms.title')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="guardian_first_name" name="guardian_first_name" />
            <label for="guardian_first_name">{{trans('forms.first_name')}}</label>
        </div>
    </div>

    {{--<div class="col s12 m4">--}}
        {{--<div class="input-field col s12">--}}
            {{--<input type="text" id="middle_name" name="middle_name" />--}}
            {{--<label for="middle_name">{{trans('forms.middle_name')}}</label>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="guardian_last_name" name="guardian_last_name" />
            <label for="guardian_last_name">{{trans('forms.last_name')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="guardian_country" name="guardian_country">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="guardian_country">{{trans('forms.country')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="guardian_state" name="guardian_state">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="guardian_state">{{trans('forms.state')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="guardian_city" name="guardian_city">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="guardian_city">{{trans('forms.city')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <div class="input-field col s12">
            <textarea id="guardian_address" class="materialize-textarea"
                      name="guardian_address" length="120"></textarea>
            <label for="guardian_address">{{trans('forms.address')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="guardian_relationship" name="guardian_relationship">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="guardian_relationship">{{trans('forms.relationship')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="guardian_email" name="guardian_email" />
            <label for="guardian_email">{{trans('forms.email')}}</label>
        </div>
    </div>

    <div class="col s12 m4">
        <div class="input-field col s12">
            <input type="text" id="guardian_phone" name="guardian_phone" />
            <label for="guardian_phone">{{trans('forms.phone')}}</label>
        </div>
    </div>
</div>