<div class="row">
    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="level" name="level">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="level">{{trans('forms.level')}}</label>
        </div>
    </div>
    <div class="col s12 m4">
        <div class="input-field col s12">
            <select id="section" name="section">
                <option value="" disabled selected>Choose ...</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <label for="section">{{trans('forms.section')}}</label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <div class="input-field col s12">
            <textarea id="former_school" class="materialize-textarea"
                      name="former_school" length="120"></textarea>
            <label for="former_school">{{trans('forms.former_school')}}</label>
        </div>
    </div>
</div>