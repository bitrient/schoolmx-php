<?php

return [

    /*
    |--------------------------------------------------------------------------
    | E-mails Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used when sending E-mails to the User.
    |
    */
    'excited' => 'We are excited to have you join School MS.',
    'excited_back' => 'we are excited to have you back.',
    'confirm_registration' => 'Please confirm your registration to continue',
    'confirm' => 'Confirm',
    'password_reset' => 'Password Reset',
    'forgot_password' => 'Forgot your password? No problem!',
    'procedure' => 'Just click the link below or copy and '.
        'paste the address in your browser to reset. On the website, you\'ll be able to enter and '.
        'confirm your new password.',
    'reset_link' => 'Reset link',
    'team' => 'Team SchoolMx'


];
