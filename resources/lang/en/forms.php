<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Form Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the application forms.
    |
    */

    'first_name' => 'First Name',
    'middle_name'     => 'Middle Name',
    'last_name'     => 'Last Name',
    'religion'  => 'Religion',
    'dob' => 'Date of Birth',
    'gender' => 'Gender',
    'blood_group' => 'Blood Group',
    'birth_place' => 'Birth Place',
    'mother_tongue' => 'Mother Tongue',
    'country' => 'Country',
    'state' => 'State',
    'city' => 'City',
    'postal_code' => 'Postal Code',
    'email' => 'Email',
    'email_address' => 'Email Address',
    'phone' => 'Phone Number',
    'address' => 'Address',
    'bio_data' => 'Bio Data',
    'additional_info' => 'Additional Info',
    'summary' => 'Summary',
    'academics' => 'Academics',
    'guardian_sibling' => 'Guardian & Sibling',
    'guardian_sibling_prompt' => 'Please select one sibling from the'
        .'list or use the search box to refined the search.',
    'siblings' => 'Sibling (s)',
    'guardian' => 'Guardian',
    'add_guardian' => 'Add Guardian',
    'search' => 'Search',
    'level' => 'Level',
    'section' => 'Section',
    'former_school' => 'Former School',
    'height' => 'Height',
    'weight' => 'Weight',
    'allergies' => 'Allergies',
    'title' => 'Title',
    'relationship' => 'Relationship'


];
