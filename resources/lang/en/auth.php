<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'oops' => 'Oops!',
    'inactive' => 'Please contact admin, your account has not been activated.',
    'expired' => 'Your session has expired. Please re-login.',

    'username' => 'Username',
    'password' => 'Password',
    'password_confirmation' => 'Verify Password',
    'login_field' => 'Email or Username',
    'login' => 'Login',
    'logout' => 'Logout',

    /**
     * Login Page
     */
    'login_prompt' => 'Please Enter your information',
    'login_button' => 'Login',
    'login_remember' => 'Remember me',
    'login_forgot' => 'Forgot my password',

    /**
     * Retrieve page
     */
    'retrieve' => 'Retrieve Password.',
    'retrieve_prompt' => 'Enter your email or username to receive instructions.',
    'retrieve_button' => 'Send Me!',
    'retrieve_back' => 'Back to Login',

    /**
     * Password Reset
     */
    'reset' => 'Password Reset',
    'reset_button' => 'Reset Password',
    'reset_prompt' => 'Please enter a new password and repeat it to reset and login.'

];
