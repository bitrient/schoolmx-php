<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the application menus.
    |
    */

    'welcome' => 'Welcome',
    'home' => 'Home',
    'overview' => 'Overview',
    'dashboard' => 'Dashboard',
    'dashboard_desc' => 'Overview &amp; statistics',
    'admissions' => 'Admissions',
    'admissions_desc' => 'Student registration',
    'last_login' => 'Last login',
    'settings' => 'Settings',
    'profile' => 'Profile',
    'academics' => 'Academics',
    'admin' => 'Admin',
    'administration' => 'Administration',
    'reports' => 'Reports',
    'hr' => 'Human Resources'

];
