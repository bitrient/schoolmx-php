$(document).ready(function() {
  $('.tooltipped').tooltip({delay: 50});
  $('#login').validate({
    rules: {
      username: {
        required: true,
        minlength: 4
      },
      password: {
        required: true,
        minlength: 4
      }
    },
    //For custom messages
    messages: {
      username:{
          required: "Please enter your Username.",
          minlength: "Please enter a valid Username."
      },
      password:{
          required: "Please enter your Password.",
          minlength: "Please enter a valid Password."
      }
    },
    errorElement : 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');

      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    },
    invalidHandler: function(event, validator) {
      Materialize.toast('Invalid login credentials!', 3000, 'red darken-4');
    }
  });

  $(".button-collapse").sideNav();
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15
  });

  $('select').material_select();

  $("*").find("a[href='"+window.location.href+"']").each(function(){
        jQuery(this).parent().addClass("active");
        //add additional code here if needed
  });

  $('#admission').wizard()
      // Triggered when clicking the Next / Prev buttons
      .on('actionclicked.fu.wizard', function(e, data) {
        if (data.direction === 'previous') {
          // Do nothing if going to the previous step.
          return;
        }
        console.log("Action .....");
      })
      .on('finished.fu.wizard', function(e) {
        console.log("Finished clicked.....");
      });
});
