<?php

namespace App\Providers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->composer('*', function ($view) {
          $view->with('__user', Sentinel::getUser());
      });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
