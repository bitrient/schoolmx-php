<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix' => 'auth'], function () {
  Route::get('/', 'Auth\AuthController@showLogin')->name('login');
  Route::post('/', 'Auth\AuthController@processLogin')->name('process_login');
  Route::get('/logout', 'Auth\AuthController@processLogout')->name('logout');
  Route::get('/forgot-password', 'Auth\AuthController@showForgotPassword')
    ->name('forgot_password');
});


  Route::get('/dashboard', 'DashboardController@showIndex')->name('dashboard');
  Route::get('/admissions', 'HR\StudentsController@create')->name('admissions');
  Route::get('/settings', 'Settings\SettingsController@showIndex')->name('settings');

