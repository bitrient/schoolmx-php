<?php

namespace App\Http\Controllers\Auth;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;

use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

  public function __construct() {
    $this->middleware('guest', ['except' => 'processLogout' ]);
  }

    /**
     * Display login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLogin()
    {
      return view('auth.login');
    }

    /**
     * Display forgot password page.
     *
     * @return \Illuminate\Http\Response
     */
     public function showForgotPassword()
     {
       return view('auth.forgot_password');
     }

    /**
     * Authenticate the User with the given credentials
     *
     * @param Request $request
     * @return Response
     */
    public function processLogin(Request $request)
    {
      // return "fdsfldsf";
        $this->validate($request, [
            'username' => 'required|max:255',
            'password' => 'required|max:255'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');
        $remember = $request->input('remember');

        $credentials = [
            'login' => $username,
            'password' => $password
        ];

        return $this->doLogin($credentials, $remember);

        // $credentials = [
        //     'email'    => 'czprobity@gmail.com',
        //     'username' => 'czprobity',
        //     'password' => 'pass',
        // ];
        //
        // $user = Sentinel::registerAndActivate($credentials);
        //
        // if ($user) {
        //   return "whooooohooo!";
        // } else {
        //   return "): sad face.";
        // }

        // dd($request->all());
        // return view('login');
    }

    /**
     * login
     *
     * does the actual login
     *
     * @param mixed[] Array of login credentials
     * @param bool Remember token
     * @return Response
     */
    private function doLogin($credentials, $remember) {

        try {

            if (Sentinel::authenticate($credentials, $remember))
            {
                return redirect()->intended('/dashboard');
            }

            return back()->withInput()
                ->withErrors(array('<strong>'.trans('auth.oops').'</strong> '. trans('auth.failed')));

        } catch (NotActivatedException $e) {

            return redirect()->route('login')->withInput()
                ->withErrors(array('<strong>'.trans('auth.oops').'</strong> '. trans('auth.inactive')));

        } catch (ThrottlingException $e) {

            $seconds = gmdate("i:s", $e->getDelay());

            return redirect()->route('login')->withInput()
                ->withErrors(array('<strong>'.trans('auth.oops').'</strong> '.
                    trans('auth.throttle', ['seconds' => $seconds])));
        }
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\Response
     */
    public function processLogout()
    {
      Sentinel::logout(null, true);
      return redirect()->route('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
