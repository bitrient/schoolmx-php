<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   *
   *
   */
  public function showIndex()
  {
    return view('dashboard');
  }
}
